package ru.osgiproject.task3;

/**
 * Created by Botsula K. on 06.02.2017.
 */
public interface GreetingInterface {
    void getGreeting();
}
