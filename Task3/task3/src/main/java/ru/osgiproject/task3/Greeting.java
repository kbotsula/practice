package ru.osgiproject.task3;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Service;

/**
 * Created by Botsula K. on 06.02.2017.
 */
@Component
@Service
public class Greeting implements GreetingInterface {

    @Activate
    void start(){
    }

    @Deactivate
    void stop(){
    }

    public void getGreeting() {
        System.out.println("Hello World!");
    }
}
