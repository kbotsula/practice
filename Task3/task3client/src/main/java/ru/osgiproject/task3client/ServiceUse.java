package ru.osgiproject.task3client;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;

import ru.osgiproject.task3.GreetingInterface;

/**
 * Created by Botsula K. on 06.02.2017.
 */
@Component
public class ServiceUse {
    @Reference(bind = "bindGreetings", unbind = "unbindGreetings")
    GreetingInterface greeting;

    public void bindGreetings(GreetingInterface greeting) {
        this.greeting = greeting;
    }

    public void unbindGreetings(GreetingInterface greeting) {
        this.greeting = null;
    }

    @Activate
    void start(){
        greeting.getGreeting();
    }

}
