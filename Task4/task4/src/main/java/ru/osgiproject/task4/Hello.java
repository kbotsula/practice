package ru.osgiproject.task4;


/**
 * Created by Botsula K. on 09.02.2017.
 */
public class Hello implements HelloInterface {

    public void hello(String param) {
        System.out.println("Hello, " + param);
    }
}
