package ru.osgiproject.task4;

/**
 * Created by Botsula K. on 09.02.2017.
 */
public interface HelloInterface {
    void hello(String param);
}
