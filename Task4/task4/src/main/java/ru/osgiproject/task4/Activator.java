package ru.osgiproject.task4;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import java.util.Hashtable;

/**
 * Created by Botsula K. on 09.02.2017.
 */
public class Activator implements BundleActivator {
    public void start(BundleContext bundleContext) throws Exception {
        Hashtable props = new Hashtable();
        props.put("osgi.command.scope", "practice");
        props.put("osgi.command.function", "hello");
        bundleContext.registerService(HelloInterface.class.getName(), new Hello(), props);
    }

    public void stop(BundleContext bundleContext) throws Exception {
    }
}