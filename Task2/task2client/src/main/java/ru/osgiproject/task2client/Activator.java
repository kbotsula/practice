package ru.osgiproject.task2client;

import ru.osgiproject.task2.GreetingInterface;
import org.osgi.framework.*;

/**
 * Created by Botsula K. on 06.02.2017.
 */
public class Activator implements BundleActivator {
    public void start(BundleContext bundleContext) throws Exception {
        ServiceReference ref =
                bundleContext.getServiceReference(GreetingInterface.class.getName());
                ((GreetingInterface) bundleContext.getService(ref)).getGreeting();
    }

    public void stop(BundleContext bundleContext) throws Exception {

    }
}
