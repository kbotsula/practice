package ru.osgiproject.task2;

/**
 * Created by Botsula K. on 06.02.2017.
 */
public interface GreetingInterface {
    void getGreeting();
}
