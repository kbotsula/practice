package ru.osgiproject.task2;


/**
 * Created by Botsula K. on 06.02.2017.
 */
public class Greeting implements GreetingInterface {

    public void getGreeting() {
        System.out.println("Hello OSGi World!");
    }
}
