package ru.osgiproject.task2;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

/**
 * Created by Botsula K. on 06.02.2017.
 */
public class Activator implements BundleActivator {
    public void start(BundleContext bundleContext) throws Exception {
        bundleContext.registerService(GreetingInterface.class.getName(),
                new Greeting(), null);
    }

    public void stop(BundleContext bundleContext) throws Exception {
    }
}
