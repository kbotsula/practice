package ru.osgiproject.task5client;

/**
 * Created by Botsula K. on 12.02.2017.
 */
public interface StatInterface {
    void stats();
    void stats(String resource);
}
