package ru.osgiproject.task5client;

import java.util.ArrayList;

/**
 * Created by Botsula K. on 12.02.2017.
 */
public interface TitleListInterface {
    ArrayList<String> getTitles();
}
