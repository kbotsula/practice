package ru.osgiproject.task5client;

import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

import java.util.*;

/**
 * Created by Botsula K. on 12.02.2017.
 */
public class NewsStat implements StatInterface {
    ServiceTracker tracker;
    ServiceReference[] references;
    SortedMap<String, Integer> stat = new TreeMap<String, Integer>();

    public NewsStat(ServiceTracker tracker) {
        this.tracker = tracker;
    }

    public void stats() {
        stats(null);
    }

    public void stats(String resource) {
        references = tracker.getServiceReferences();
        stat.clear();
        if (resource != null) {
            CreateStat(resource);
        } else {
            Scanner scan = new Scanner(System.in);
            System.out.println("Chose api:");
            System.out.println("1) lenta");
            System.out.println("2) aif");
            System.out.println("3) all");
            int api = scan.nextInt();
            if (api == 1) {
                CreateStat("lenta");
            } else if (api == 2) {
                CreateStat("aif");
            } else if (api == 3) {
                CreateStat("lenta");
                CreateStat("aif");
            }
        }
        if (stat.isEmpty()) {
            System.out.println("Resource:" + resource + " not available");
        } else {
            SortedSet<Map.Entry<String, Integer>> sortedStat = MapSort(stat);
            List list = new ArrayList(stat.entrySet());
            Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {

                @Override
                public int compare(Map.Entry<String, Integer> e1, Map.Entry<String, Integer> e2) {
                    return e1.getValue().compareTo(e2.getValue());
                }

            });
            int i = 0;
            for (Map.Entry<String, Integer> entry : sortedStat) {
                if (i == 10) {
                    break;
                } else {
                    System.out.println((i + 1) + ": " + entry.getKey() + ": " + entry.getValue());
                    i++;
                }
            }
        }
    }

    private void CreateStat(String api) {
        try {
            for (int i = 0; i < references.length; i++) {
                if (references[i].getBundle().getSymbolicName().equals("task5" + api)) {
                    CreateServiceStat(references[i]);
                }
            }
        } catch (NullPointerException ex) {
            System.out.println("Services not found");
        }
    }

    private void CreateServiceStat(ServiceReference reference) {
        TitleListInterface service = (TitleListInterface) tracker.getService(reference);
        ArrayList<String> titles = service.getTitles();
        for (int i = 0; i < titles.size(); i++) {
            String title = titles.get(i).replaceAll("[\\u00ab\\u00bb!?,\"/:#.]", "");
            String[] words = title.split("\\s+");
            AddWords(words);
        }
    }

    private void AddWords(String[] words) {
        for (int i = 0; i < words.length; i++) {
            if (stat.get(words[i]) != null) {
                stat.put(words[i], stat.get(words[i]) + 1);
            } else {
                stat.put(words[i], 1);
            }
        }
    }

    private static <K,V extends Comparable<? super V>>
    SortedSet<Map.Entry<K,V>> MapSort(Map<K,V> map) {
        SortedSet<Map.Entry<K,V>> sortedMap = new TreeSet<Map.Entry<K,V>>(
                new Comparator<Map.Entry<K,V>>() {
                    @Override public int compare(Map.Entry<K,V> e1, Map.Entry<K,V> e2) {
                        int res = e2.getValue().compareTo(e1.getValue());
                        return res != 0 ? res : 1;
                    }
                }
        );
        sortedMap.addAll(map.entrySet());
        return sortedMap;
    }
}
