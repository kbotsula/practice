package ru.osgiproject.task5client;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import java.util.Hashtable;

/**
 * Created by Botsula K. on 12.02.2017.
 */
public class Activator implements BundleActivator {
    ServiceTracker tracker;

    public void start(BundleContext bundleContext) throws Exception {
        Hashtable props = new Hashtable();
        props.put("osgi.command.scope", "news");
        props.put("osgi.command.function", "stats");
        tracker = new ServiceTracker(bundleContext, TitleListInterface.class.getName(), null);
        tracker.open();
        bundleContext.registerService(StatInterface.class.getName(), new NewsStat(tracker), props);
    }

    public void stop(BundleContext bundleContext) throws Exception {
        tracker.close();
    }
}
