package ru.osgiproject.task5lenta;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import ru.osgiproject.task5client.TitleListInterface;

/**
 * Created by Botsula K. on 12.02.2017.
 */
public class Activator implements BundleActivator {
    public void start(BundleContext bundleContext) throws Exception {
        bundleContext.registerService(TitleListInterface.class.getName(),
                new LentaApi(), null);
    }

    public void stop(BundleContext bundleContext) throws Exception {
    }
}