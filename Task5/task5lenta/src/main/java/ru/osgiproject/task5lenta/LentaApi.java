package ru.osgiproject.task5lenta;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.osgiproject.task5client.TitleListInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Botsula K. on 12.02.2017.
 */
public class LentaApi implements TitleListInterface {
    String urlString;
    ArrayList<String> titles;

    public LentaApi() {
        titles = new ArrayList<String>();
        urlString = "https://api.lenta.ru/lists/latest";
    }

    private String getJsonString() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            URL url = new URL(urlString);
            String inputLine;
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
            while ((inputLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
        } catch (IOException ex) {
            System.out.println("Bad connection");
            ex.printStackTrace(System.out);
        }
        return stringBuilder.toString();
    }

    public ArrayList<String> getTitles() {
        titles.clear();
        JSONObject jsonObject = new JSONObject(getJsonString());
        JSONArray jsonArray = jsonObject.getJSONArray("headlines");
        for (int i = 0; i < jsonArray.length(); ++i) {
            final JSONObject news = jsonArray.getJSONObject(i).getJSONObject("info");
            titles.add(news.getString("title").toLowerCase());
        }
        return titles;
    }
}
