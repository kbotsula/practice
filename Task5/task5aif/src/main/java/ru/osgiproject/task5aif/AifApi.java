package ru.osgiproject.task5aif;

import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import ru.osgiproject.task5client.TitleListInterface;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Botsula K. on 12.02.2017.
 */
public class AifApi implements TitleListInterface {
    String urlString;
    ArrayList<String> titles;

    public AifApi() {
        titles = new ArrayList<String>();
        urlString = "http://www.aif.ru/rss/news.php";
    }

    public ArrayList<String> getTitles() {
        titles.clear();
        try {
            URL url = new URL(urlString);
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(url.openStream());
            document.getDocumentElement().normalize();
            NodeList nodeList = document.getElementsByTagName("title");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    titles.add(node.getTextContent().toLowerCase()); //replaceAll("\<!\[CDATA\[", "")
                }
            }
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace(System.out);
        } catch (SAXException ex) {
            ex.printStackTrace(System.out);
        } catch (IOException ex) {
            System.out.println("Bad connection");
            ex.printStackTrace(System.out);
        }
        return titles;
    }
}
